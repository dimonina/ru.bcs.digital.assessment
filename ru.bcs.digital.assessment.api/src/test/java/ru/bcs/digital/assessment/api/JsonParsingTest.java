package ru.bcs.digital.assessment.api;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;
import static org.junit.jupiter.params.provider.Arguments.of;
import static ru.bcs.digital.assessment.api.matchers.Matchers.greater;
import static ru.bcs.digital.assessment.api.matchers.Matchers.greaterOrEquals;
import static ru.bcs.digital.assessment.api.matchers.Matchers.isEqualTo;
import static ru.bcs.digital.assessment.api.matchers.Matchers.isNotEqualTo;
import static ru.bcs.digital.assessment.api.matchers.Matchers.lower;
import static ru.bcs.digital.assessment.api.matchers.Matchers.lowerOrEquals;
import static ru.bcs.digital.assessment.api.rules.Rules.claim;
import static ru.bcs.digital.assessment.api.rules.Rules.debtor;
import static ru.bcs.digital.assessment.api.rules.Rules.salary;

import java.io.IOException;
import java.util.stream.Stream;

import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;

/**
 * Test cases boundary values:
 * <p>
 * id:
 * <ul>
 * <li>{@code Long#MIN_VALUE} - valid;
 * <li>{@code -1} - valid;
 * <li>{@code 0} - valid;
 * <li>{@code 1} - valid;
 * <li>{@code Long#MAX_VALUE} - valid;
 * <li>{@code null} - valid;
 * <li>{@code <not specified>} - valid;
 * </ul>
 * <p>
 * rule type:
 * <ul>
 * <li>{@code claim} - valid;
 * <li>{@code salary} - valid;
 * <li>{@code debtor} - valid;
 * <li>{@code null} - invalid, throws {@code JsonMappingException};
 * <li>{@code <not specified>} - invalid, throws {@code JsonMappingException};
 * <li>{@code <unknown>} - invalid, throws {@code JsonMappingException};
 * </ul>
 * <p>
 * matcher type:
 * <ul>
 * <li>{@code greater} - valid;
 * <li>{@code greater_or_equals} - valid;
 * <li>{@code lower} - valid;
 * <li>{@code lower_or_equals} - valid;
 * <li>{@code equals} - valid;
 * <li>{@code not_equals} - valid;
 * <li>{@code null} - invalid, throws {@code JsonMappingException};
 * <li>{@code <not specified>} - invalid, throws {@code JsonMappingException};
 * <li>{@code <unknown>} - invalid, throws {@code JsonMappingException};
 * </ul>
 * <p>
 * matcher numeric values:
 * <ul>
 * <li>{@code Double#MIN_VALUE} - valid;
 * <li>{@code -1.0} - valid;
 * <li>{@code 0.0} - valid;
 * <li>{@code 1.0} - valid;
 * <li>{@code Double#MAX_VALUE} - valid;
 * <li>{@code null} - invalid, throws {@code NullPointerException};
 * <li>{@code <not specified>} - invalid, throws {@code NullPointerException};
 * </ul>
 * <p>
 * matcher boolean values:
 * <ul>
 * <li>{@code Boolean#TRUE} - valid;
 * <li>{@code Boolean#FALSE} - valid;
 * <li>{@code null} - invalid, throws {@code NullPointerException};
 * <li>{@code <not specified>} - invalid, throws {@code NullPointerException};
 * </ul>
 * <p>
 * There are special cases for numbers with different scales, e.g. 0 and 0.000,
 * they should be equals.
 */
public class JsonParsingTest {

    private final static Long ID_MIN = Long.MIN_VALUE;
    private final static Long ID_$ONE = -1L;
    private final static Long ID_ZERO = 0L;
    private final static Long ID_ONE = 1L;
    private final static Long ID_MAX = Long.MAX_VALUE;
    private final static Long ID_NULL = null;

    private final static Double VAL_MIN = Double.MIN_VALUE;
    private final static Double VAL_$ONE = -1.0;
    private final static Double VAL_ZERO = 0.0;
    private final static Double VAL_ONE = 1.0;
    private final static Double VAL_MAX = Double.MAX_VALUE;
    private final static Double VAL_NULL = null;

    private final static Boolean DEB_TRUE = Boolean.TRUE;
    private final static Boolean DEB_FALSE = Boolean.FALSE;
    private final static Boolean DEB_NULL = null;

    private final static String RULE_UNKNOWN = "unknown_rule";
    private final static String RULE_NULL = null;

    private final static String MATCHER_UNKNOWN = "unknown_matcher";
    private final static String MATCHER_NULL = null;

    static Stream<Arguments> validTestCases() {
        // @formatter:off 
        return Stream.of( 
            // +-------------------------------------------------------------------------------------------------------------------------+-----------------------------------------+
            // | JSON                                                                                                                    | Parsed                                  |
            // +-------------------------------------------------------------------------------------------------------------------------+-----------------------------------------+
            // Vary ID for ClaimRule 
            of("{\"id\":" + ID_MIN +  ",\"type\":\"claim\",\"matcher\":{\"operator\":\"greater\",\"value\":" + VAL_ONE + "}}",            claim(ID_MIN,  greater(VAL_ONE)) ),
            of("{\"id\":" + ID_$ONE + ",\"type\":\"claim\",\"matcher\":{\"operator\":\"greater\",\"value\":" + VAL_ONE + "}}",            claim(ID_$ONE, greater(VAL_ONE)) ),
            of("{\"id\":" + ID_ZERO + ",\"type\":\"claim\",\"matcher\":{\"operator\":\"greater\",\"value\":" + VAL_ONE + "}}",            claim(ID_ZERO, greater(VAL_ONE)) ),
            of("{\"id\":" + ID_ONE +  ",\"type\":\"claim\",\"matcher\":{\"operator\":\"greater\",\"value\":" + VAL_ONE + "}}",            claim(ID_ONE,  greater(VAL_ONE)) ),
            of("{\"id\":" + ID_MAX +  ",\"type\":\"claim\",\"matcher\":{\"operator\":\"greater\",\"value\":" + VAL_ONE + "}}",            claim(ID_MAX,  greater(VAL_ONE)) ),
            of("{\"id\":" + ID_NULL + ",\"type\":\"claim\",\"matcher\":{\"operator\":\"greater\",\"value\":" + VAL_ONE + "}}",            claim(ID_NULL, greater(VAL_ONE)) ),
            of("{\"type\":\"claim\",\"matcher\":{\"operator\":\"greater\",\"value\":" + VAL_ONE + "}}",                                   claim(ID_NULL, greater(VAL_ONE)) ),
            // Vary value for ClaimRule + BigDecimalGreaterThenMatcher                                                                                     
            of("{\"id\":" + ID_ONE + ",\"type\":\"claim\",\"matcher\":{\"operator\":\"greater\",\"value\":" + VAL_MIN + "}}",             claim(ID_ONE, greater(VAL_MIN)) ),
            of("{\"id\":" + ID_ONE + ",\"type\":\"claim\",\"matcher\":{\"operator\":\"greater\",\"value\":" + VAL_$ONE + "}}",            claim(ID_ONE, greater(VAL_$ONE)) ),
            of("{\"id\":" + ID_ONE + ",\"type\":\"claim\",\"matcher\":{\"operator\":\"greater\",\"value\":" + VAL_ZERO + "}}",            claim(ID_ONE, greater(VAL_ZERO)) ),
            of("{\"id\":" + ID_ONE + ",\"type\":\"claim\",\"matcher\":{\"operator\":\"greater\",\"value\":" + VAL_ONE + "}}",             claim(ID_ONE, greater(VAL_ONE)) ),
            of("{\"id\":" + ID_ONE + ",\"type\":\"claim\",\"matcher\":{\"operator\":\"greater\",\"value\":" + VAL_MAX + "}}",             claim(ID_ONE, greater(VAL_MAX)) ),
            // Vary value for ClaimRule + BigDecimalGreaterThenOrEqualsMatcher                                                                             
            of("{\"id\":" + ID_ONE + ",\"type\":\"claim\",\"matcher\":{\"operator\":\"greater_or_equals\",\"value\":" + VAL_MIN + "}}",   claim(ID_ONE, greaterOrEquals(VAL_MIN)) ),
            of("{\"id\":" + ID_ONE + ",\"type\":\"claim\",\"matcher\":{\"operator\":\"greater_or_equals\",\"value\":" + VAL_$ONE + "}}",  claim(ID_ONE, greaterOrEquals(VAL_$ONE)) ),
            of("{\"id\":" + ID_ONE + ",\"type\":\"claim\",\"matcher\":{\"operator\":\"greater_or_equals\",\"value\":" + VAL_ZERO + "}}",  claim(ID_ONE, greaterOrEquals(VAL_ZERO)) ),
            of("{\"id\":" + ID_ONE + ",\"type\":\"claim\",\"matcher\":{\"operator\":\"greater_or_equals\",\"value\":" + VAL_ONE + "}}",   claim(ID_ONE, greaterOrEquals(VAL_ONE)) ),
            of("{\"id\":" + ID_ONE + ",\"type\":\"claim\",\"matcher\":{\"operator\":\"greater_or_equals\",\"value\":" + VAL_MAX + "}}",   claim(ID_ONE, greaterOrEquals(VAL_MAX)) ),
            // Vary value for ClaimRule + BigDecimalLowerThenMatcher                                                                                       
            of("{\"id\":" + ID_ONE + ",\"type\":\"claim\",\"matcher\":{\"operator\":\"lower\",\"value\":" + VAL_MIN + "}}",               claim(ID_ONE, lower(VAL_MIN)) ),
            of("{\"id\":" + ID_ONE + ",\"type\":\"claim\",\"matcher\":{\"operator\":\"lower\",\"value\":" + VAL_$ONE + "}}",              claim(ID_ONE, lower(VAL_$ONE)) ),
            of("{\"id\":" + ID_ONE + ",\"type\":\"claim\",\"matcher\":{\"operator\":\"lower\",\"value\":" + VAL_ZERO + "}}",              claim(ID_ONE, lower(VAL_ZERO)) ),
            of("{\"id\":" + ID_ONE + ",\"type\":\"claim\",\"matcher\":{\"operator\":\"lower\",\"value\":" + VAL_ONE + "}}",               claim(ID_ONE, lower(VAL_ONE)) ),
            of("{\"id\":" + ID_ONE + ",\"type\":\"claim\",\"matcher\":{\"operator\":\"lower\",\"value\":" + VAL_MAX + "}}",               claim(ID_ONE, lower(VAL_MAX)) ),
            // Vary value for ClaimRule + BigDecimalLowerThenOrEqualsMatcher                                                                               
            of("{\"id\":" + ID_ONE + ",\"type\":\"claim\",\"matcher\":{\"operator\":\"lower_or_equals\",\"value\":" + VAL_MIN + "}}",     claim(ID_ONE, lowerOrEquals(VAL_MIN)) ),
            of("{\"id\":" + ID_ONE + ",\"type\":\"claim\",\"matcher\":{\"operator\":\"lower_or_equals\",\"value\":" + VAL_$ONE + "}}",    claim(ID_ONE, lowerOrEquals(VAL_$ONE)) ),
            of("{\"id\":" + ID_ONE + ",\"type\":\"claim\",\"matcher\":{\"operator\":\"lower_or_equals\",\"value\":" + VAL_ZERO + "}}",    claim(ID_ONE, lowerOrEquals(VAL_ZERO)) ),
            of("{\"id\":" + ID_ONE + ",\"type\":\"claim\",\"matcher\":{\"operator\":\"lower_or_equals\",\"value\":" + VAL_ONE + "}}",     claim(ID_ONE, lowerOrEquals(VAL_ONE)) ),
            of("{\"id\":" + ID_ONE + ",\"type\":\"claim\",\"matcher\":{\"operator\":\"lower_or_equals\",\"value\":" + VAL_MAX + "}}",     claim(ID_ONE, lowerOrEquals(VAL_MAX)) ),
            // Vary value for ClaimRule + ObjectEqualsMatcher                                                                               
            of("{\"id\":" + ID_ONE + ",\"type\":\"claim\",\"matcher\":{\"operator\":\"equals\",\"value\":" + VAL_MIN + "}}",              claim(ID_ONE, isEqualTo(VAL_MIN)) ),
            of("{\"id\":" + ID_ONE + ",\"type\":\"claim\",\"matcher\":{\"operator\":\"equals\",\"value\":" + VAL_$ONE + "}}",             claim(ID_ONE, isEqualTo(VAL_$ONE)) ),
            of("{\"id\":" + ID_ONE + ",\"type\":\"claim\",\"matcher\":{\"operator\":\"equals\",\"value\":" + VAL_ZERO + "}}",             claim(ID_ONE, isEqualTo(VAL_ZERO)) ),
            of("{\"id\":" + ID_ONE + ",\"type\":\"claim\",\"matcher\":{\"operator\":\"equals\",\"value\":" + VAL_ONE + "}}",              claim(ID_ONE, isEqualTo(VAL_ONE)) ),
            of("{\"id\":" + ID_ONE + ",\"type\":\"claim\",\"matcher\":{\"operator\":\"equals\",\"value\":" + VAL_MAX + "}}",              claim(ID_ONE, isEqualTo(VAL_MAX)) ),
            // Vary value for ClaimRule + ObjectNotEqualsMatcher                                                                               
            of("{\"id\":" + ID_ONE + ",\"type\":\"claim\",\"matcher\":{\"operator\":\"not_equals\",\"value\":" + VAL_MIN + "}}",          claim(ID_ONE, isNotEqualTo(VAL_MIN)) ),
            of("{\"id\":" + ID_ONE + ",\"type\":\"claim\",\"matcher\":{\"operator\":\"not_equals\",\"value\":" + VAL_$ONE + "}}",         claim(ID_ONE, isNotEqualTo(VAL_$ONE)) ),
            of("{\"id\":" + ID_ONE + ",\"type\":\"claim\",\"matcher\":{\"operator\":\"not_equals\",\"value\":" + VAL_ZERO + "}}",         claim(ID_ONE, isNotEqualTo(VAL_ZERO)) ),
            of("{\"id\":" + ID_ONE + ",\"type\":\"claim\",\"matcher\":{\"operator\":\"not_equals\",\"value\":" + VAL_ONE + "}}",          claim(ID_ONE, isNotEqualTo(VAL_ONE)) ),
            of("{\"id\":" + ID_ONE + ",\"type\":\"claim\",\"matcher\":{\"operator\":\"not_equals\",\"value\":" + VAL_MAX + "}}",          claim(ID_ONE, isNotEqualTo(VAL_MAX)) ),

            // Vary ID for SalaryRule 
            of("{\"id\":" + ID_MIN +  ",\"type\":\"salary\",\"matcher\":{\"operator\":\"greater\",\"value\":" + VAL_ONE + "}}",           salary(ID_MIN,  greater(VAL_ONE)) ),
            of("{\"id\":" + ID_$ONE + ",\"type\":\"salary\",\"matcher\":{\"operator\":\"greater\",\"value\":" + VAL_ONE + "}}",           salary(ID_$ONE, greater(VAL_ONE)) ),
            of("{\"id\":" + ID_ZERO + ",\"type\":\"salary\",\"matcher\":{\"operator\":\"greater\",\"value\":" + VAL_ONE + "}}",           salary(ID_ZERO, greater(VAL_ONE)) ),
            of("{\"id\":" + ID_ONE +  ",\"type\":\"salary\",\"matcher\":{\"operator\":\"greater\",\"value\":" + VAL_ONE + "}}",           salary(ID_ONE,  greater(VAL_ONE)) ),
            of("{\"id\":" + ID_MAX +  ",\"type\":\"salary\",\"matcher\":{\"operator\":\"greater\",\"value\":" + VAL_ONE + "}}",           salary(ID_MAX,  greater(VAL_ONE)) ),
            of("{\"id\":" + ID_NULL + ",\"type\":\"salary\",\"matcher\":{\"operator\":\"greater\",\"value\":" + VAL_ONE + "}}",           salary(ID_NULL, greater(VAL_ONE)) ),
            of("{\"type\":\"salary\",\"matcher\":{\"operator\":\"greater\",\"value\":" + VAL_ONE + "}}",                                  salary(ID_NULL, greater(VAL_ONE)) ),
            // Vary value for SalaryRule + BigDecimalGreaterThenMatcher 
            of("{\"id\":" + ID_ONE + ",\"type\":\"salary\",\"matcher\":{\"operator\":\"greater\",\"value\":" + VAL_MIN + "}}",            salary(ID_ONE, greater(VAL_MIN)) ),
            of("{\"id\":" + ID_ONE + ",\"type\":\"salary\",\"matcher\":{\"operator\":\"greater\",\"value\":" + VAL_$ONE + "}}",           salary(ID_ONE, greater(VAL_$ONE)) ),
            of("{\"id\":" + ID_ONE + ",\"type\":\"salary\",\"matcher\":{\"operator\":\"greater\",\"value\":" + VAL_ZERO + "}}",           salary(ID_ONE, greater(VAL_ZERO)) ),
            of("{\"id\":" + ID_ONE + ",\"type\":\"salary\",\"matcher\":{\"operator\":\"greater\",\"value\":" + VAL_ONE + "}}",            salary(ID_ONE, greater(VAL_ONE)) ),
            of("{\"id\":" + ID_ONE + ",\"type\":\"salary\",\"matcher\":{\"operator\":\"greater\",\"value\":" + VAL_MAX + "}}",            salary(ID_ONE, greater(VAL_MAX)) ),
            // Vary value for SalaryRule + BigDecimalGreaterThenOrEqualsMatcher
            of("{\"id\":" + ID_ONE + ",\"type\":\"salary\",\"matcher\":{\"operator\":\"greater_or_equals\",\"value\":" + VAL_MIN + "}}",  salary(ID_ONE, greaterOrEquals(VAL_MIN)) ),
            of("{\"id\":" + ID_ONE + ",\"type\":\"salary\",\"matcher\":{\"operator\":\"greater_or_equals\",\"value\":" + VAL_$ONE + "}}", salary(ID_ONE, greaterOrEquals(VAL_$ONE)) ),
            of("{\"id\":" + ID_ONE + ",\"type\":\"salary\",\"matcher\":{\"operator\":\"greater_or_equals\",\"value\":" + VAL_ZERO + "}}", salary(ID_ONE, greaterOrEquals(VAL_ZERO)) ),
            of("{\"id\":" + ID_ONE + ",\"type\":\"salary\",\"matcher\":{\"operator\":\"greater_or_equals\",\"value\":" + VAL_ONE + "}}",  salary(ID_ONE, greaterOrEquals(VAL_ONE)) ),
            of("{\"id\":" + ID_ONE + ",\"type\":\"salary\",\"matcher\":{\"operator\":\"greater_or_equals\",\"value\":" + VAL_MAX + "}}",  salary(ID_ONE, greaterOrEquals(VAL_MAX)) ),
            // Vary value for SalaryRule + BigDecimalLowerThenMatcher 
            of("{\"id\":" + ID_ONE + ",\"type\":\"salary\",\"matcher\":{\"operator\":\"lower\",\"value\":" + VAL_MIN + "}}",              salary(ID_ONE, lower(VAL_MIN)) ),
            of("{\"id\":" + ID_ONE + ",\"type\":\"salary\",\"matcher\":{\"operator\":\"lower\",\"value\":" + VAL_$ONE + "}}",             salary(ID_ONE, lower(VAL_$ONE)) ),
            of("{\"id\":" + ID_ONE + ",\"type\":\"salary\",\"matcher\":{\"operator\":\"lower\",\"value\":" + VAL_ZERO + "}}",             salary(ID_ONE, lower(VAL_ZERO)) ),
            of("{\"id\":" + ID_ONE + ",\"type\":\"salary\",\"matcher\":{\"operator\":\"lower\",\"value\":" + VAL_ONE + "}}",              salary(ID_ONE, lower(VAL_ONE)) ),
            of("{\"id\":" + ID_ONE + ",\"type\":\"salary\",\"matcher\":{\"operator\":\"lower\",\"value\":" + VAL_MAX + "}}",              salary(ID_ONE, lower(VAL_MAX)) ),
            // Vary value for SalaryRule + BigDecimalLowerThenOrEqualsMatcher
            of("{\"id\":" + ID_ONE + ",\"type\":\"salary\",\"matcher\":{\"operator\":\"lower_or_equals\",\"value\":" + VAL_MIN + "}}",    salary(ID_ONE, lowerOrEquals(VAL_MIN)) ),
            of("{\"id\":" + ID_ONE + ",\"type\":\"salary\",\"matcher\":{\"operator\":\"lower_or_equals\",\"value\":" + VAL_$ONE + "}}",   salary(ID_ONE, lowerOrEquals(VAL_$ONE)) ),
            of("{\"id\":" + ID_ONE + ",\"type\":\"salary\",\"matcher\":{\"operator\":\"lower_or_equals\",\"value\":" + VAL_ZERO + "}}",   salary(ID_ONE, lowerOrEquals(VAL_ZERO)) ),
            of("{\"id\":" + ID_ONE + ",\"type\":\"salary\",\"matcher\":{\"operator\":\"lower_or_equals\",\"value\":" + VAL_ONE + "}}",    salary(ID_ONE, lowerOrEquals(VAL_ONE)) ),
            of("{\"id\":" + ID_ONE + ",\"type\":\"salary\",\"matcher\":{\"operator\":\"lower_or_equals\",\"value\":" + VAL_MAX + "}}",    salary(ID_ONE, lowerOrEquals(VAL_MAX)) ),
            // Vary value for SalaryRule + ObjectEqualsMatcher                                                                               
            of("{\"id\":" + ID_ONE + ",\"type\":\"salary\",\"matcher\":{\"operator\":\"equals\",\"value\":" + VAL_MIN + "}}",             salary(ID_ONE, isEqualTo(VAL_MIN)) ),
            of("{\"id\":" + ID_ONE + ",\"type\":\"salary\",\"matcher\":{\"operator\":\"equals\",\"value\":" + VAL_$ONE + "}}",            salary(ID_ONE, isEqualTo(VAL_$ONE)) ),
            of("{\"id\":" + ID_ONE + ",\"type\":\"salary\",\"matcher\":{\"operator\":\"equals\",\"value\":" + VAL_ZERO + "}}",            salary(ID_ONE, isEqualTo(VAL_ZERO)) ),
            of("{\"id\":" + ID_ONE + ",\"type\":\"salary\",\"matcher\":{\"operator\":\"equals\",\"value\":" + VAL_ONE + "}}",             salary(ID_ONE, isEqualTo(VAL_ONE)) ),
            of("{\"id\":" + ID_ONE + ",\"type\":\"salary\",\"matcher\":{\"operator\":\"equals\",\"value\":" + VAL_MAX + "}}",             salary(ID_ONE, isEqualTo(VAL_MAX)) ),
            // Vary value for SalaryRule + ObjectNotEqualsMatcher                                                                              
            of("{\"id\":" + ID_ONE + ",\"type\":\"salary\",\"matcher\":{\"operator\":\"not_equals\",\"value\":" + VAL_MIN + "}}",         salary(ID_ONE, isNotEqualTo(VAL_MIN)) ),
            of("{\"id\":" + ID_ONE + ",\"type\":\"salary\",\"matcher\":{\"operator\":\"not_equals\",\"value\":" + VAL_$ONE + "}}",        salary(ID_ONE, isNotEqualTo(VAL_$ONE)) ),
            of("{\"id\":" + ID_ONE + ",\"type\":\"salary\",\"matcher\":{\"operator\":\"not_equals\",\"value\":" + VAL_ZERO + "}}",        salary(ID_ONE, isNotEqualTo(VAL_ZERO)) ),
            of("{\"id\":" + ID_ONE + ",\"type\":\"salary\",\"matcher\":{\"operator\":\"not_equals\",\"value\":" + VAL_ONE + "}}",         salary(ID_ONE, isNotEqualTo(VAL_ONE)) ),
            of("{\"id\":" + ID_ONE + ",\"type\":\"salary\",\"matcher\":{\"operator\":\"not_equals\",\"value\":" + VAL_MAX + "}}",         salary(ID_ONE, isNotEqualTo(VAL_MAX)) ),

            // Vary ID for DebtorRule 
            of("{\"id\":" + ID_MIN +  ",\"type\":\"debtor\",\"matcher\":{\"operator\":\"equals\",\"value\":" + DEB_TRUE + "}}",           debtor(ID_MIN,  isEqualTo(DEB_TRUE)) ),
            of("{\"id\":" + ID_$ONE + ",\"type\":\"debtor\",\"matcher\":{\"operator\":\"equals\",\"value\":" + DEB_TRUE + "}}",           debtor(ID_$ONE, isEqualTo(DEB_TRUE)) ),
            of("{\"id\":" + ID_ZERO + ",\"type\":\"debtor\",\"matcher\":{\"operator\":\"equals\",\"value\":" + DEB_TRUE + "}}",           debtor(ID_ZERO, isEqualTo(DEB_TRUE)) ),
            of("{\"id\":" + ID_ONE +  ",\"type\":\"debtor\",\"matcher\":{\"operator\":\"equals\",\"value\":" + DEB_TRUE + "}}",           debtor(ID_ONE,  isEqualTo(DEB_TRUE)) ),
            of("{\"id\":" + ID_MAX +  ",\"type\":\"debtor\",\"matcher\":{\"operator\":\"equals\",\"value\":" + DEB_TRUE + "}}",           debtor(ID_MAX,  isEqualTo(DEB_TRUE)) ),
            of("{\"id\":" + ID_NULL + ",\"type\":\"debtor\",\"matcher\":{\"operator\":\"equals\",\"value\":" + DEB_TRUE + "}}",           debtor(ID_NULL, isEqualTo(DEB_TRUE)) ),
            of("{\"type\":\"debtor\",\"matcher\":{\"operator\":\"equals\",\"value\":" + DEB_TRUE + "}}",                                  debtor(ID_NULL, isEqualTo(DEB_TRUE)) ),
            // Vary value for DebtorRule + ObjectEqualsMatcher
            of("{\"id\":" + ID_ONE + ",\"type\":\"debtor\",\"matcher\":{\"operator\":\"equals\",\"value\":" + DEB_TRUE + "}}",            debtor(ID_ONE, isEqualTo(DEB_TRUE)) ),
            of("{\"id\":" + ID_ONE + ",\"type\":\"debtor\",\"matcher\":{\"operator\":\"equals\",\"value\":" + DEB_FALSE + "}}",           debtor(ID_ONE, isEqualTo(DEB_FALSE)) ),
            // Vary value for DebtorRule + ObjectNotEqualsMatcher                                                                              
            of("{\"id\":" + ID_ONE + ",\"type\":\"debtor\",\"matcher\":{\"operator\":\"not_equals\",\"value\":" + DEB_TRUE + "}}",        debtor(ID_ONE, isNotEqualTo(DEB_TRUE)) ),
            of("{\"id\":" + ID_ONE + ",\"type\":\"debtor\",\"matcher\":{\"operator\":\"not_equals\",\"value\":" + DEB_FALSE + "}}",       debtor(ID_ONE, isNotEqualTo(DEB_FALSE)) ),
            
            // Vary 0 and 0.000 values for ClaimRule + matcher                                                                               
            of("{\"id\":" + ID_ONE + ",\"type\":\"claim\",\"matcher\":{\"operator\":\"not_equals\",\"value\":0}}",                        claim(ID_ONE, isNotEqualTo(VAL_ZERO)) ), // Check 0
            of("{\"id\":" + ID_ONE + ",\"type\":\"claim\",\"matcher\":{\"operator\":\"not_equals\",\"value\":0.000}}",                    claim(ID_ONE, isNotEqualTo(VAL_ZERO)) ), // Check 0.000
            of("{\"id\":" + ID_ONE + ",\"type\":\"claim\",\"matcher\":{\"operator\":\"lower_or_equals\",\"value\":0}}",                   claim(ID_ONE, lowerOrEquals(VAL_ZERO)) ), // Check 0
            of("{\"id\":" + ID_ONE + ",\"type\":\"claim\",\"matcher\":{\"operator\":\"lower_or_equals\",\"value\":0.000}}",               claim(ID_ONE, lowerOrEquals(VAL_ZERO)) ), // Check 0.000
            of("{\"id\":" + ID_ONE + ",\"type\":\"claim\",\"matcher\":{\"operator\":\"equals\",\"value\":0}}",                            claim(ID_ONE, isEqualTo(VAL_ZERO)) ), // Check 0
            of("{\"id\":" + ID_ONE + ",\"type\":\"claim\",\"matcher\":{\"operator\":\"equals\",\"value\":0.000}}",                        claim(ID_ONE, isEqualTo(VAL_ZERO)) ), // Check 0.000
            of("{\"id\":" + ID_ONE + ",\"type\":\"claim\",\"matcher\":{\"operator\":\"lower\",\"value\":0}}",                             claim(ID_ONE, lower(VAL_ZERO)) ), // Check 0
            of("{\"id\":" + ID_ONE + ",\"type\":\"claim\",\"matcher\":{\"operator\":\"lower\",\"value\":0.000}}",                         claim(ID_ONE, lower(VAL_ZERO)) ), // Check 0.000
            of("{\"id\":" + ID_ONE + ",\"type\":\"claim\",\"matcher\":{\"operator\":\"greater_or_equals\",\"value\":0}}",                 claim(ID_ONE, greaterOrEquals(VAL_ZERO)) ), // Check 0
            of("{\"id\":" + ID_ONE + ",\"type\":\"claim\",\"matcher\":{\"operator\":\"greater_or_equals\",\"value\":0.000}}",             claim(ID_ONE, greaterOrEquals(VAL_ZERO)) ), // Check 0.000
            of("{\"id\":" + ID_ONE + ",\"type\":\"claim\",\"matcher\":{\"operator\":\"greater\",\"value\":0}}",                           claim(ID_ONE, greater(VAL_ZERO)) ), // Check 0
            of("{\"id\":" + ID_ONE + ",\"type\":\"claim\",\"matcher\":{\"operator\":\"greater\",\"value\":0.000}}",                       claim(ID_ONE, greater(VAL_ZERO)) ) // Check 0.000
        // @formatter:on 
        );
    }

    static Stream<Arguments> invalidTestCasesJsonMappingException() {
        // @formatter:off 
        return Stream.of( 
            // +------------------------------------------------------------------------------------------------------------------------+
            // | JSON                                                                                                                   |
            // +------------------------------------------------------------------------------------------------------------------------+
            of("{\"id\":" + ID_ONE + ",\"type\":" + RULE_NULL + ",\"matcher\":{\"operator\":\"greater\",\"value\":" + VAL_ONE + "}}" ),         // null rule type
            of("{\"id\":" + ID_ONE + ",\"matcher\":{\"operator\":\"greater\",\"value\":" + VAL_ONE + "}}" ),                                    // missed rule type
            of("{\"id\":" + ID_MAX + ",\"type\":\"" + RULE_UNKNOWN + "\",\"matcher\":{\"operator\":\"greater\",\"value\":" + VAL_ONE + "}}" ),  // unknown rule type
            
            of("{\"id\":" + ID_ONE + ",\"type\":\"claim\",\"matcher\":{\"operator\":" + MATCHER_NULL + ",\"value\":" + VAL_ONE + "}}" ),        // null matcher type  
            of("{\"id\":" + ID_ONE + ",\"type\":\"claim\",\"matcher\":{\"value\":" + VAL_ONE + "}}" ),                                      // missed matcher type
            of("{\"id\":" + ID_ONE + ",\"type\":\"claim\",\"matcher\":{\"operator\":\"" + MATCHER_UNKNOWN + "\",\"value\":" + VAL_ONE + "}}" )  // unknown matcher type
        // @formatter:on 
        );
    }

    static Stream<Arguments> invalidTestCasesJsonParseException() {
        // @formatter:off 
        return Stream.of( 
            // +------------------------------------------------------------------------------------------------------------------------+
            // | JSON                                                                                                                   |
            // +------------------------------------------------------------------------------------------------------------------------+
            of("{id\":" + ID_MIN +  ",\"type\":\"claim,\"matcher\":{\"operator\":\"greater\",\"value\":" + VAL_ONE + "}}" ),
            of("plain text" )
        // @formatter:on 
        );
    }

    static Stream<Arguments> invalidTestCasesNullPointerException() {
        // @formatter:off 
        return Stream.of( 
            // +---------------------------------------------------------------------------------------------------------------------+
            // | JSON                                                                                                                |
            // +---------------------------------------------------------------------------------------------------------------------+
            // Vary ID for ClaimRule 
            of("{\"id\":" + ID_ONE + ",\"type\":\"claim\",\"matcher\":{\"operator\":\"greater\",\"value\":" + VAL_NULL + "}}" ),
            of("{\"id\":" + ID_ONE + ",\"type\":\"claim\",\"matcher\":{\"operator\":\"greater_or_equals\",\"value\":" + VAL_NULL + "}}" ),
            of("{\"id\":" + ID_ONE + ",\"type\":\"claim\",\"matcher\":{\"operator\":\"lower\",\"value\":" + VAL_NULL + "}}" ),
            of("{\"id\":" + ID_ONE + ",\"type\":\"claim\",\"matcher\":{\"operator\":\"lower_or_equals\",\"value\":" + VAL_NULL + "}}" ),
            of("{\"id\":" + ID_ONE + ",\"type\":\"claim\",\"matcher\":{\"operator\":\"equals\",\"value\":" + VAL_NULL + "}}" ),
            of("{\"id\":" + ID_ONE + ",\"type\":\"claim\",\"matcher\":{\"operator\":\"not_equals\",\"value\":" + VAL_NULL + "}}" ),
            of("{\"id\":" + ID_ONE + ",\"type\":\"debtor\",\"matcher\":{\"operator\":\"equals\",\"value\":" + DEB_NULL + "}}" ),
            of("{\"id\":" + ID_ONE + ",\"type\":\"debtor\",\"matcher\":{\"operator\":\"not_equals\",\"value\":" + DEB_NULL + "}}" ),
            
            of("{\"id\":" + ID_ONE + ",\"type\":\"claim\",\"matcher\":{\"operator\":\"greater\"}}" ),
            of("{\"id\":" + ID_ONE + ",\"type\":\"claim\",\"matcher\":{\"operator\":\"greater_or_equals\"}}" ),
            of("{\"id\":" + ID_ONE + ",\"type\":\"claim\",\"matcher\":{\"operator\":\"lower\"}}" ),
            of("{\"id\":" + ID_ONE + ",\"type\":\"claim\",\"matcher\":{\"operator\":\"lower_or_equals\"}}" ),
            of("{\"id\":" + ID_ONE + ",\"type\":\"claim\",\"matcher\":{\"operator\":\"equals\"}}" ),
            of("{\"id\":" + ID_ONE + ",\"type\":\"claim\",\"matcher\":{\"operator\":\"not_equals\"}}" ),
            of("{\"id\":" + ID_ONE + ",\"type\":\"debtor\",\"matcher\":{\"operator\":\"equals\"}}" ),
            of("{\"id\":" + ID_ONE + ",\"type\":\"debtor\",\"matcher\":{\"operator\":\"not_equals\"}}" )
        // @formatter:on 
        );
    }

    private ObjectMapper mapper = new ObjectMapper();

    @ParameterizedTest
    @MethodSource("validTestCases")
    public void shouldReadValidString(String json, Rule expected)
            throws JsonParseException, JsonMappingException, IOException {
        // When
        Rule rule = mapper.readValue(json, Rule.class);
        // Then
        assertThat(rule).isEqualTo(expected);
    }

    @ParameterizedTest
    @MethodSource("validTestCases")
    public void shouldWriteAndReadTheSame(String expected, Rule rule)
            throws JsonParseException, JsonMappingException, IOException {
        // When
        String json = mapper.writeValueAsString(rule);
        Rule ruleAfter = mapper.readValue(json, Rule.class);
        // Then
        assertThat(ruleAfter).isEqualTo(rule);
    }

    @ParameterizedTest
    @MethodSource("invalidTestCasesJsonMappingException")
    public void shouldThrowJsonMappingException(String json)
            throws JsonParseException, JsonMappingException, IOException {
        // When Then
        assertThatThrownBy(() -> mapper.readValue(json, Rule.class))
                .isInstanceOf(JsonMappingException.class);
    }

    @ParameterizedTest
    @MethodSource("invalidTestCasesJsonParseException")
    public void shouldThrowJsonParseException(String json)
            throws JsonParseException, JsonMappingException, IOException {
        // When Then
        assertThatThrownBy(() -> mapper.readValue(json, Rule.class))
                .isInstanceOf(JsonParseException.class);
    }

    @ParameterizedTest
    @MethodSource("invalidTestCasesNullPointerException")
    public void shouldThrowNullPointerException(String json)
            throws JsonParseException, JsonMappingException, IOException {
        // When Then
        assertThatThrownBy(() -> mapper.readValue(json, Rule.class))
                .isInstanceOf(JsonMappingException.class)
                .hasCauseInstanceOf(NullPointerException.class);
    }

}
