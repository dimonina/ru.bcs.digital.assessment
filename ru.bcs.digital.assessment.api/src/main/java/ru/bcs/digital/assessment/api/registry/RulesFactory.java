package ru.bcs.digital.assessment.api.registry;

import ru.bcs.digital.assessment.api.Matcher;
import ru.bcs.digital.assessment.api.Rule;
import ru.bcs.digital.assessment.api.dto.RuleDto;

public interface RulesFactory {

    public Rule create(RuleDto dto, Matcher<?> matcher);

}