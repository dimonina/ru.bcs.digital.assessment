package ru.bcs.digital.assessment.api.registry;

import ru.bcs.digital.assessment.api.Matcher;
import ru.bcs.digital.assessment.api.dto.MatcherDto;

public interface MatchersFactory {

    public Matcher<?> create(MatcherDto dto);

}