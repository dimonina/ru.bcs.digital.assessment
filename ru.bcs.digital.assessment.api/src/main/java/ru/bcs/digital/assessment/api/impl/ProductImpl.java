package ru.bcs.digital.assessment.api.impl;

import java.math.BigDecimal;
import java.time.Period;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Builder;
import lombok.Value;
import ru.bcs.digital.assessment.api.Product;
import ru.bcs.digital.assessment.api.Rule;

/**
 * Default immutable implementation of {@code Product}.
 */
@Value
@Builder
public class ProductImpl implements Product {

    private final Long id;

    private final String name;

    private final BigDecimal percent;

    private final Period period;

    private final List<Rule> rules;

    @JsonCreator
    public ProductImpl(@JsonProperty("id") Long id, @JsonProperty("name") String name,
            @JsonProperty("percent") BigDecimal percent, @JsonProperty("period") Period period,
            @JsonProperty("rules") List<Rule> rules) {
        this.id = id;
        this.name = name;
        this.percent = percent;
        this.period = period;
        this.rules = rules;
    }

}
