package ru.bcs.digital.assessment.api.matchers;

import java.math.BigDecimal;
import java.util.Objects;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NonNull;
import ru.bcs.digital.assessment.api.Matcher;

@Data
@AllArgsConstructor
public abstract class AbstractBigDecimalMatcher implements Matcher<BigDecimal> {

    @NonNull
    private final BigDecimal value;
    
    @Override
    public String getType() {
        return "number";
    }

    /**
     * <p>
     * Override lombok's generated equals, because
     * {@code BigDecimal.valueOf(0).equals(BigDecimal.valueOf(0.0)) == false}.
     * Use {@code Comparable#compareTo(Object)} instead of
     * {@code Object#equals(Object)}.
     */
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (obj == this) {
            return true;
        }
        if (obj.getClass() == this.getClass()) {
            AbstractBigDecimalMatcher other = (AbstractBigDecimalMatcher) obj;
            if (other.value.compareTo(value) == 0) {
                return true;
            }
        }
        return false;
    }

    @Override
    public int hashCode() {
        return Objects.hash(value);
    }

    /**
     * To check.
     */
    public static void main(String[] args) {
        System.out.println(BigDecimal.valueOf(0).equals(BigDecimal.valueOf(0.0)));
    }

}
