package ru.bcs.digital.assessment.api.matchers;

import java.math.BigDecimal;

import lombok.experimental.UtilityClass;

@UtilityClass
public class Matchers {

    // ----- BigDecimalGreaterThenMatcher -----

    public BigDecimalGreaterThenMatcher greater(double bound) {
        return greater(BigDecimal.valueOf(bound));
    }

    public BigDecimalGreaterThenMatcher greater(BigDecimal bound) {
        return new BigDecimalGreaterThenMatcher(bound);
    }

    // ----- BigDecimalGreaterThenOrEqualsMatcher -----

    public BigDecimalGreaterThenOrEqualsMatcher greaterOrEquals(double bound) {
        return greaterOrEquals(BigDecimal.valueOf(bound));
    }

    public BigDecimalGreaterThenOrEqualsMatcher greaterOrEquals(BigDecimal bound) {
        return new BigDecimalGreaterThenOrEqualsMatcher(bound);
    }

    // ----- BigDecimalLowerThenMatcher -----

    public BigDecimalLowerThenMatcher lower(double bound) {
        return lower(BigDecimal.valueOf(bound));
    }

    public BigDecimalLowerThenMatcher lower(BigDecimal bound) {
        return new BigDecimalLowerThenMatcher(bound);
    }

    // ----- BigDecimalLowerThenOrEqualsMatcher -----

    public BigDecimalLowerThenOrEqualsMatcher lowerOrEquals(double bound) {
        return lowerOrEquals(BigDecimal.valueOf(bound));
    }

    public BigDecimalLowerThenOrEqualsMatcher lowerOrEquals(BigDecimal bound) {
        return new BigDecimalLowerThenOrEqualsMatcher(bound);
    }

    // ----- ObjectEqualsMatcher -----

    public ObjectEqualsMatcher isEqualTo(Object bound) {
        return new ObjectEqualsMatcher(bound);
    }

    // ----- ObjectNotEqualsMatcher -----

    public ObjectNotEqualsMatcher isNotEqualTo(Object bound) {
        return new ObjectNotEqualsMatcher(bound);
    }

}
