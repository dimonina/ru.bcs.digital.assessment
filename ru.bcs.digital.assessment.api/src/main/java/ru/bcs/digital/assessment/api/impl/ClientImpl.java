package ru.bcs.digital.assessment.api.impl;

import java.math.BigDecimal;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Builder;
import lombok.Value;
import ru.bcs.digital.assessment.api.Client;

/**
 * Default immutable implementation of {@code Client}.
 */
@Value
@Builder
public class ClientImpl implements Client {

    private final BigDecimal salary;

    private final BigDecimal claim;

    private final boolean debtor;

    @JsonCreator
    public ClientImpl(@JsonProperty("salary") BigDecimal salary,
            @JsonProperty("claim") BigDecimal claim, @JsonProperty("is_debtor") boolean debtor) {
        this.salary = salary;
        this.claim = claim;
        this.debtor = debtor;
    }

}
