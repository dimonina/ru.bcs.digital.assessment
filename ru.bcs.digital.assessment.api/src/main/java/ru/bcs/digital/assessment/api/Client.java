package ru.bcs.digital.assessment.api;

import java.math.BigDecimal;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonTypeInfo;
import com.fasterxml.jackson.annotation.JsonTypeInfo.Id;

import ru.bcs.digital.assessment.api.impl.ClientImpl;

/**
 * Represent client object.
 */
@JsonTypeInfo(defaultImpl = ClientImpl.class, use = Id.MINIMAL_CLASS)
public interface Client {

    /**
     * Retrieve salary of client.
     * 
     * @return salary of client
     */
    @JsonProperty("salary")
    BigDecimal getSalary();

    /**
     * Retrieve claim for credit of client.
     * 
     * @return claim for credit of client
     */
    @JsonProperty("claim")
    BigDecimal getClaim();

    /**
     * Retrieve flag indicating that client is debtor.
     * 
     * @return flag indicating that client is debtor
     */
    @JsonProperty("is_debtor")
    boolean isDebtor();

}
