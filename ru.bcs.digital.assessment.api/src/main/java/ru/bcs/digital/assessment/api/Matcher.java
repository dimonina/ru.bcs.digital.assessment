package ru.bcs.digital.assessment.api;

import java.util.function.Predicate;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonSubTypes;
import com.fasterxml.jackson.annotation.JsonTypeInfo;
import com.fasterxml.jackson.annotation.JsonTypeInfo.As;

import ru.bcs.digital.assessment.api.matchers.BigDecimalGreaterThenMatcher;
import ru.bcs.digital.assessment.api.matchers.BigDecimalGreaterThenOrEqualsMatcher;
import ru.bcs.digital.assessment.api.matchers.BigDecimalLowerThenMatcher;
import ru.bcs.digital.assessment.api.matchers.BigDecimalLowerThenOrEqualsMatcher;
import ru.bcs.digital.assessment.api.matchers.ObjectEqualsMatcher;
import ru.bcs.digital.assessment.api.matchers.ObjectNotEqualsMatcher;

/**
 * Represent matcher of the rule.
 * 
 * @param <T> the type of values for matching
 */
@JsonTypeInfo(use = JsonTypeInfo.Id.NAME, include = As.PROPERTY, property = "operator")
@JsonSubTypes({
        @JsonSubTypes.Type(name = BigDecimalGreaterThenMatcher.OPERATOR, value = BigDecimalGreaterThenMatcher.class),
        @JsonSubTypes.Type(name = BigDecimalGreaterThenOrEqualsMatcher.OPERATOR, value = BigDecimalGreaterThenOrEqualsMatcher.class),
        @JsonSubTypes.Type(name = BigDecimalLowerThenMatcher.OPERATOR, value = BigDecimalLowerThenMatcher.class),
        @JsonSubTypes.Type(name = BigDecimalLowerThenOrEqualsMatcher.OPERATOR, value = BigDecimalLowerThenOrEqualsMatcher.class),
        @JsonSubTypes.Type(name = ObjectEqualsMatcher.OPERATOR, value = ObjectEqualsMatcher.class),
        @JsonSubTypes.Type(name = ObjectNotEqualsMatcher.OPERATOR, value = ObjectNotEqualsMatcher.class)})
public interface Matcher<T> extends Predicate<T> {

    /**
     * Retrieve bounding value, e.g. for greater of equals matchers.
     * 
     * @return bounding value, can't be null
     */
    @JsonProperty("value")
    T getValue();

    /**
     * Retrieve a type of the matcher.
     * <p>
     * Note. Ignore this property.
     * 
     * @return a type of the matcher
     */
    @JsonIgnore
    String getType();

    /**
     * Retrieve a operator of the matcher.
     * <p>
     * Note. {@code JsonTypeInfo} will add this property.
     * 
     * @return a operator of the matcher
     */
    @JsonIgnore
    String getOperator();

    /**
     * Retrieve string representation of value, used for persistent layer.
     * 
     * @return string representation of value, can't be null
     */
    @JsonIgnore
    default String getValueString() {
        return getValue().toString();
    }

}
