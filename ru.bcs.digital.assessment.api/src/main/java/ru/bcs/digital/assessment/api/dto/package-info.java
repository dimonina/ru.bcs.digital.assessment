/**
 * Package contains data transfer objects for model objects.
 * <p>
 * This DTO's used in factories. Underline layers can create model object
 * without knowledge about concrete types.
 */
package ru.bcs.digital.assessment.api.dto;