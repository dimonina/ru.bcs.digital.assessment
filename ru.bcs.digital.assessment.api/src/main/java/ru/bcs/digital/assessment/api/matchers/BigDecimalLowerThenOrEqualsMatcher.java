package ru.bcs.digital.assessment.api.matchers;

import java.math.BigDecimal;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.EqualsAndHashCode;
import lombok.ToString;
import lombok.Value;
import ru.bcs.digital.assessment.api.dto.MatcherDto;

@Value
@ToString(callSuper = true)
@EqualsAndHashCode(callSuper = true)
public class BigDecimalLowerThenOrEqualsMatcher extends AbstractBigDecimalMatcher {

    public static final String OPERATOR = "lower_or_equals";

    @JsonCreator
    public BigDecimalLowerThenOrEqualsMatcher(@JsonProperty("value") BigDecimal bound) {
        super(bound);
    }

    public BigDecimalLowerThenOrEqualsMatcher(MatcherDto dto) {
        super(new BigDecimal(dto.getValue()));
    }

    @Override
    public boolean test(BigDecimal value) {
        return value.compareTo(getValue()) <= 0;
    }
    
    @Override
    public String getOperator() {
        return OPERATOR;
    }

}
