package ru.bcs.digital.assessment.api.rules;

import java.math.BigDecimal;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Data;
import lombok.EqualsAndHashCode;
import ru.bcs.digital.assessment.api.Client;
import ru.bcs.digital.assessment.api.Matcher;
import ru.bcs.digital.assessment.api.dto.RuleDto;

@Data
@EqualsAndHashCode(callSuper = true)
public class ClaimRule extends AbstractRule {

    public static final String TYPE = "claim";

    private Matcher<? super BigDecimal> matcher;

    @JsonCreator
    public ClaimRule(@JsonProperty("id") Long id,
            @JsonProperty("matcher") Matcher<? super BigDecimal> matcher) {
        super(id);
        this.matcher = matcher;
    }

    @SuppressWarnings("unchecked")
    public ClaimRule(RuleDto dto, Matcher<?> matcher) {
        this(dto.getId(), (Matcher<? super BigDecimal>) matcher);
    }

    @Override
    public boolean test(Client client) {
        return matcher.test(client.getClaim());
    }

    @Override
    public String getType() {
        return TYPE;
    }

}
