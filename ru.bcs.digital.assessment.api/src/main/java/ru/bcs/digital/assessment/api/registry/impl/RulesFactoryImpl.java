package ru.bcs.digital.assessment.api.registry.impl;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.function.BiFunction;

import lombok.Getter;
import ru.bcs.digital.assessment.api.Matcher;
import ru.bcs.digital.assessment.api.Rule;
import ru.bcs.digital.assessment.api.dto.RuleDto;
import ru.bcs.digital.assessment.api.registry.RulesFactory;

@Getter
public class RulesFactoryImpl implements RulesFactory {

    private final Map<String, BiFunction<RuleDto, Matcher<?>, Rule>> registry = new HashMap<>();

    public Rule create(RuleDto dto, Matcher<?> matcher) {
        return registry.get(dto.getType()).apply(dto, matcher);
    }

    public void register(String type, BiFunction<RuleDto, Matcher<?>, Rule> supplier) {
        registry.put(type, supplier);
    }

    public void unregister(String type) {
        registry.remove(type);
    }

    public Map<String, BiFunction<RuleDto, Matcher<?>, Rule>> getRegistry() {
        return Collections.unmodifiableMap(registry);
    }

}
