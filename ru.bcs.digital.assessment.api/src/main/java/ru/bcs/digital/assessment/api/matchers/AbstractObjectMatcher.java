package ru.bcs.digital.assessment.api.matchers;

import java.math.BigDecimal;
import java.util.Objects;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NonNull;
import ru.bcs.digital.assessment.api.Matcher;

@Data
@AllArgsConstructor
public abstract class AbstractObjectMatcher implements Matcher<Object> {

    @NonNull
    private final Object value;
    
    @Override
    public String getType() {
        return "object";
    }

    /**
     * <p>
     * Implementation has specific compare method for {@code Number} values. If
     * value is Number, then wrap it to {@code BigDecimal} and compare them. If
     * values are not numbers, then {@code Object#equals(Object)} will be used.
     * <p>
     * Override lombok's generated equals, because
     * {@code BigDecimal.valueOf(0).equals(BigDecimal.valueOf(0.0)) == false}.
     */
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (obj == this) {
            return true;
        }
        if (obj.getClass() == this.getClass()) {
            AbstractObjectMatcher other = (AbstractObjectMatcher) obj;
            if (other.value instanceof Number && value instanceof Number) {
                Number a = (Number) other.value;
                Number b = (Number) value;
                return new BigDecimal(a.toString()).compareTo(new BigDecimal(b.toString())) == 0;
            } else {
                return other.value.equals(value);
            }
        }
        return false;
    }

    @Override
    public int hashCode() {
        if (value instanceof Number) {
            return Objects.hash(new BigDecimal(((Number) value).toString()));
        } else {
            return Objects.hash(value);
        }
    }

}
