package ru.bcs.digital.assessment.api;

import java.util.function.Predicate;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonSubTypes;
import com.fasterxml.jackson.annotation.JsonTypeInfo;
import com.fasterxml.jackson.annotation.JsonTypeInfo.As;

import ru.bcs.digital.assessment.api.rules.ClaimRule;
import ru.bcs.digital.assessment.api.rules.DebtorRule;
import ru.bcs.digital.assessment.api.rules.SalaryRule;

/**
 * Represent rule applied for the {@code Product}.
 */
@JsonTypeInfo(use = JsonTypeInfo.Id.NAME, include = As.PROPERTY, property = "type")
@JsonSubTypes({
        @JsonSubTypes.Type(value = ClaimRule.class, name = ClaimRule.TYPE),
        @JsonSubTypes.Type(value = SalaryRule.class, name = SalaryRule.TYPE),
        @JsonSubTypes.Type(value = DebtorRule.class, name = DebtorRule.TYPE)})
public interface Rule extends Predicate<Client> {

    /**
     * Retrieve unique identifier of the rule.
     * 
     * @return unique identifier of the rule, can be null if received from
     *         client application, assume that generated value will be applied
     *         when store to storage
     */
    @JsonProperty("id")
    Long getId();

    /**
     * Retrieve matcher of the rule.
     * 
     * @return matcher of the rule
     */
    @JsonProperty("matcher")
    Matcher<?> getMatcher();

    /**
     * Retrieve a type of the rule.
     * <p>
     * Note. {@code JsonTypeInfo} will add this property.
     * 
     * @return a type of the rule
     */
    @JsonIgnore
    String getType();

}
