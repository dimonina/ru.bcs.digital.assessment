package ru.bcs.digital.assessment.api.rules;

import lombok.Data;
import ru.bcs.digital.assessment.api.Rule;

@Data
public abstract class AbstractRule implements Rule {

    protected final Long id;

}
