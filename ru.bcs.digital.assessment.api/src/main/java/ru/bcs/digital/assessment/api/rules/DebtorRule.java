package ru.bcs.digital.assessment.api.rules;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Data;
import lombok.EqualsAndHashCode;
import ru.bcs.digital.assessment.api.Client;
import ru.bcs.digital.assessment.api.Matcher;
import ru.bcs.digital.assessment.api.dto.RuleDto;

@Data
@EqualsAndHashCode(callSuper = true)
public class DebtorRule extends AbstractRule {

    public static final String TYPE = "debtor";

    private Matcher<? super Boolean> matcher;

    @JsonCreator
    public DebtorRule(@JsonProperty("id") Long id,
            @JsonProperty("matcher") Matcher<? super Boolean> matcher) {
        super(id);
        this.matcher = matcher;
    }

    @SuppressWarnings("unchecked")
    public DebtorRule(RuleDto dto, Matcher<?> matcher) {
        this(dto.getId(), (Matcher<? super Boolean>) matcher);
    }

    @Override
    public boolean test(Client client) {
        return matcher.test(client.isDebtor());
    }

    @Override
    public String getType() {
        return TYPE;
    }

}
