package ru.bcs.digital.assessment.api.dto;

import java.sql.Timestamp;

import lombok.Data;

/**
 * Data transfer object for {@code Matcher}.
 */
@Data
public class MatcherDto {

    private Long id;

    private Timestamp dateCreate;

    private String type;

    private String value;

    private String operator;

}
