/**
 * Package contains default implementations for factories for API model objects.
 */
package ru.bcs.digital.assessment.api.registry.impl;