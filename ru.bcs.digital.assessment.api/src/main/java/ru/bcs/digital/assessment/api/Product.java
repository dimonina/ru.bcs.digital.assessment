package ru.bcs.digital.assessment.api;

import java.math.BigDecimal;
import java.time.Period;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;

import ru.bcs.digital.assessment.api.json.PeriodDeserializer;
import ru.bcs.digital.assessment.api.json.PeriodSerializer;

/**
 * Represent product object.
 */
public interface Product {

    /**
     * Retrieve unique identifier of the product.
     * 
     * @return unique identifier of the product
     */
    @JsonProperty("id")
    Long getId();

    /**
     * Retrieve name of the product.
     * 
     * @return name of the product
     */
    @JsonProperty("name")
    String getName();

    /**
     * Retrieve percent of the product.
     * 
     * @return percent of the product
     */
    @JsonProperty("percent")
    BigDecimal getPercent();

    /**
     * Retrieve period of the product.
     * 
     * @return
     */
    @JsonProperty("period")
    @JsonSerialize(using = PeriodSerializer.class)
    @JsonDeserialize(using = PeriodDeserializer.class)
    Period getPeriod();

    /**
     * Retrieve rules applied for the product.
     * 
     * @return rules applied for the product
     */
    @JsonProperty("rules")
    List<Rule> getRules();

}
