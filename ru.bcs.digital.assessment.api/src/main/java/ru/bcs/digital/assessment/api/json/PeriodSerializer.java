package ru.bcs.digital.assessment.api.json;

import java.io.IOException;
import java.time.Period;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonSerializer;
import com.fasterxml.jackson.databind.SerializerProvider;

public class PeriodSerializer extends JsonSerializer<Period> {

    @Override
    public void serialize(Period value, JsonGenerator jgen, SerializerProvider provider)
            throws IOException, JsonProcessingException {
        jgen.writeString(value.toString());
    }

}
