package ru.bcs.digital.assessment.api.dto;

import java.sql.Timestamp;

import lombok.Data;

/**
 * Data transfer object for {@code Rule}.
 */
@Data
public class RuleDto {

    private Long id;

    private Timestamp dateCreate;

    private Long productId;

    private String type;

    private Long matcherId;

}
