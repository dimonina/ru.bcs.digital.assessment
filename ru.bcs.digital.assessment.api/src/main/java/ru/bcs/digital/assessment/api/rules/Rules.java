package ru.bcs.digital.assessment.api.rules;

import java.math.BigDecimal;

import lombok.experimental.UtilityClass;
import ru.bcs.digital.assessment.api.Matcher;

@UtilityClass
public class Rules {

    public ClaimRule claim(Long id, Matcher<? super BigDecimal> rule) {
        return new ClaimRule(id, rule);
    }

    public SalaryRule salary(Long id, Matcher<? super BigDecimal> rule) {
        return new SalaryRule(id, rule);
    }

    public DebtorRule debtor(Long id, Matcher<? super Boolean> rule) {
        return new DebtorRule(id, rule);
    }

}
