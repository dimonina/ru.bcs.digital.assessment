package ru.bcs.digital.assessment.api.matchers;

import java.util.Objects;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.EqualsAndHashCode;
import lombok.ToString;
import lombok.Value;
import ru.bcs.digital.assessment.api.dto.MatcherDto;

@Value
@ToString(callSuper = true)
@EqualsAndHashCode(callSuper = true)
public class ObjectNotEqualsMatcher extends AbstractObjectMatcher {

    public static final String OPERATOR = "not_equals";

    @JsonCreator
    public ObjectNotEqualsMatcher(@JsonProperty("value") Object bound) {
        super(bound);
    }

    public ObjectNotEqualsMatcher(MatcherDto dto) {
        super(dto.getValue());
    }

    @Override
    public boolean test(Object value) {
        return !Objects.equals(value, getValue());
    }

    @Override
    public String getOperator() {
        return OPERATOR;
    }

}
