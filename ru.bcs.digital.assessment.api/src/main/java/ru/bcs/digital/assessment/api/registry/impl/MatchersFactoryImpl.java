package ru.bcs.digital.assessment.api.registry.impl;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.function.Function;

import ru.bcs.digital.assessment.api.Matcher;
import ru.bcs.digital.assessment.api.dto.MatcherDto;
import ru.bcs.digital.assessment.api.registry.MatchersFactory;

public class MatchersFactoryImpl implements MatchersFactory {

    private final Map<String, Function<MatcherDto, Matcher<?>>> registry = new HashMap<>();

    public void register(String type, Function<MatcherDto, Matcher<?>> supplier) {
        registry.put(type, supplier);
    }

    public void unregister(String type) {
        registry.remove(type);
    }

    public Map<String, Function<MatcherDto, Matcher<?>>> getRegistry() {
        return Collections.unmodifiableMap(registry);
    }

    @Override
    public Matcher<?> create(MatcherDto dto) {
        return registry.get(dto.getOperator()).apply(dto);
    }

}
