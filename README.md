# Assessment for BCS Digital

Project contains assessment project code base.

## Build

To build project:

```bash
gradle clean build -PdbHost=<host> -PdbPort=<port> -PdbName=<name> -PdbUser=<user> -PdbPassword=<password>

```
where:
- dbHost - host name of database;
- dbPort - database port;
- dbName - database name;
- dbUser - database user name;
- dbPassword - database user password;

## Local deployment example

First run postgres 10 via Docker:
```bash
sudo docker run \
	--name bcs-postgres \
	-e POSTGRES_USER=bcs \
	-e POSTGRES_PASSWORD=bcs \
	-e POSTGRES_DB=bcs \
	-p 5432:5432 \
	--rm \
	-d postgres:10-alpine
```
Then build sources:
```bash
gradle clean build -PdbHost=localhost -PdbPort=5432 -PdbName=bcs -PdbUser=bcs -PdbPassword=bcs
```
Then run application:
```bash
java -jar ./ru.bcs.digital.assessment.app/build/ru.bcs.digital.assessment.app.jar
```
Then in the different bash check:
```bash
curl http://localhost:8080/products/
```
It should answer:
```json
[{"id":1,"name":"тестовый продукт 1","percent":6.00,"period":"P36M"},{"id":2,"name":"тестовый продукт 2","percent":15.00,"period":"P0D"},{"id":3,"name":"тестовый продукт 3","percent":12.00,"period":"P60M"}]
```