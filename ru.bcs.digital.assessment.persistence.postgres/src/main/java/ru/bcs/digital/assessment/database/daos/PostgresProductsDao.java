package ru.bcs.digital.assessment.database.daos;

import static ru.bcs.digital.assessment.database.Tables.MATCHERS;
import static ru.bcs.digital.assessment.database.Tables.PRODUCTS_;
import static ru.bcs.digital.assessment.database.Tables.RULES;

import java.time.Period;
import java.util.List;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;

import org.jooq.DSLContext;
import org.jooq.Record;
import org.jooq.Result;
import org.jooq.impl.DSL;

import com.google.common.cache.Cache;
import com.google.common.cache.CacheBuilder;

import lombok.AllArgsConstructor;
import lombok.SneakyThrows;
import ru.bcs.digital.assessment.api.Matcher;
import ru.bcs.digital.assessment.api.Product;
import ru.bcs.digital.assessment.api.Rule;
import ru.bcs.digital.assessment.api.dto.MatcherDto;
import ru.bcs.digital.assessment.api.dto.RuleDto;
import ru.bcs.digital.assessment.api.impl.ProductImpl;
import ru.bcs.digital.assessment.api.registry.MatchersFactory;
import ru.bcs.digital.assessment.api.registry.RulesFactory;
import ru.bcs.digital.assessment.database.tables.records.RulesRecord;
import ru.bcs.digital.assessment.persistence.ProductsDao;

@AllArgsConstructor
public class PostgresProductsDao implements ProductsDao {

    private final DSLContext context;

    private final MatchersFactory matchersFactory;

    private final RulesFactory rulesFactory;

    private final Cache<Long, List<Rule>> rulesCache = CacheBuilder
            .newBuilder()
            .expireAfterWrite(1, TimeUnit.MINUTES)
            .build();

    @Override
    public List<Product> getProducts() {
        return context.select().from(PRODUCTS_).fetch().map(
                rec -> ProductImpl
                        .builder()
                        .id(rec.get(PRODUCTS_.ID))
                        .name(rec.get(PRODUCTS_.NAME))
                        .percent(rec.get(PRODUCTS_.PERCENT))
                        .period(Period.ofMonths(rec.get(PRODUCTS_.PERIOD)))
                        .build());
    }

    @Override
    @SneakyThrows
    public List<Rule> getProductRules(long productId) {
        return rulesCache.get(productId, () -> {
            Result<Record> rulesAndMatchers = context
                    .selectDistinct()
                    .from(RULES)
                    .join(MATCHERS)
                    .on(RULES.MATCHER_ID.equal(MATCHERS.ID))
                    .where(RULES.PRODUCT_ID.equal(productId))
                    .fetch();
            return rulesAndMatchers.stream().map(record -> {
                MatcherDto matcherDto = extractMatcherDto(record);
                RuleDto ruleDto = extractRuleDto(record);
                Matcher<?> matcher = matchersFactory.create(matcherDto);
                Rule rule = rulesFactory.create(ruleDto, matcher);
                return rule;
            }).collect(Collectors.toList());
        });
    }

    @Override
    public Long addProductRule(long productId, Rule rule) {
        return context.transactionResult(internal -> {
            Matcher<?> matcher = rule.getMatcher();
            long matcherId = DSL
                    .using(internal)
                    .insertInto(MATCHERS)
                    .columns(MATCHERS.TYPE, MATCHERS.OPERATOR, MATCHERS.VALUE)
                    .values(matcher.getType(), matcher.getOperator(), matcher.getValueString())
                    .returning()
                    .fetchOne()
                    .getId();
            RulesRecord fetch = DSL
                    .using(internal)
                    .insertInto(RULES)
                    .columns(RULES.TYPE, RULES.PRODUCT_ID, RULES.MATCHER_ID)
                    .values(rule.getType(), productId, matcherId)
                    .returning()
                    .fetchOne();
            if (fetch == null) {
                throw new IllegalArgumentException();
            }
            Long result = fetch.getId();
            rulesCache.invalidate(productId);
            return result;
        });
    }

    @Override
    public void deleteProductRule(long productId, long ruleId) {
        rulesCache.invalidate(productId);
        context.transaction(internal -> {
            Result<RulesRecord> fetch = DSL
                    .using(internal)
                    .deleteFrom(RULES)
                    .where(RULES.ID.equal(ruleId))
                    .returning(RULES.MATCHER_ID)
                    .fetch();
            if (fetch.isEmpty()) {
                throw new IllegalArgumentException("Rule with id " + ruleId + " not found");
            }
            long matcherId = fetch.get(0).getMatcherId();
            DSL.using(internal).deleteFrom(MATCHERS).where(MATCHERS.ID.equal(matcherId)).execute();
        });
    }

    private MatcherDto extractMatcherDto(Record record) {
        MatcherDto result = new MatcherDto();
        result.setId(record.get(MATCHERS.ID));
        result.setDateCreate(record.get(MATCHERS.DATE_CREATE));
        result.setOperator(record.get(MATCHERS.OPERATOR));
        result.setType(record.get(MATCHERS.TYPE));
        result.setValue(record.get(MATCHERS.VALUE));
        return result;
    }

    private RuleDto extractRuleDto(Record record) {
        RuleDto result = new RuleDto();
        result.setId(record.get(RULES.ID));
        result.setDateCreate(record.get(RULES.DATE_CREATE));
        result.setProductId(record.get(RULES.PRODUCT_ID));
        result.setType(record.get(RULES.TYPE));
        result.setMatcherId(record.get(RULES.MATCHER_ID));
        return result;
    }

    @Override
    public void invalidateCache() {
        rulesCache.invalidateAll();
    }

}
