--liquibase formatted sql

--changeset shishkin:schemas/products logicalFilePath:/

CREATE SCHEMA products;

--rollback DROP SCHEMA products;
