--liquibase formatted sql

--changeset shishkin:initial-product logicalFilePath:/

SET search_path TO products;

CREATE SEQUENCE products_id_seq;

CREATE TABLE products (
    id BIGINT PRIMARY KEY DEFAULT nextval('products_id_seq'),
	name VARCHAR(255) NULL,
	date_create TIMESTAMP NOT NULL DEFAULT now(),
	percent DECIMAL(12,2) NOT NULL,
	period DECIMAL(3, 0) NOT NULL
);

COMMENT ON COLUMN products.id IS 'Уникальный идентификатор кредитного продукта';
COMMENT ON COLUMN products.name IS 'Название кредитного продукта';
COMMENT ON COLUMN products.date_create IS 'Дата создания кредитного продукта';
COMMENT ON COLUMN products.percent IS 'Процентная ставка кредитного продукта';
COMMENT ON COLUMN products.period IS 'Срок кредитного продукта в месяцах';
--rollback DROP TABLE products;

