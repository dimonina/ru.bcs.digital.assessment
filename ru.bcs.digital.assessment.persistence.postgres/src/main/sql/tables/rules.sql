--liquibase formatted sql

--changeset shishkin:tables/rules logicalFilePath:/ splitStatements:false

SET search_path TO products;

CREATE SEQUENCE rules_id_seq;

CREATE TABLE rules (
    id BIGINT PRIMARY KEY DEFAULT nextval('rules_id_seq'),
	date_create TIMESTAMP NOT NULL DEFAULT now(),
	product_id BIGINT NOT NULL REFERENCES products(id),
	type VARCHAR(100) NOT NULL,
	matcher_id BIGINT NOT NULL
);

CREATE TABLE rules_salary (
	PRIMARY KEY (id),
    FOREIGN KEY (product_id) REFERENCES products(id),
    FOREIGN KEY (matcher_id) REFERENCES matchers_num(id),
    CHECK (type = 'salary')
) INHERITS (rules);

CREATE TABLE rules_claim  (
	PRIMARY KEY (id),
    FOREIGN KEY (product_id) REFERENCES products(id),
    FOREIGN KEY (matcher_id) REFERENCES matchers_num(id),
    CHECK (type = 'claim')
) INHERITS (rules);

CREATE TABLE rules_debtor (
	PRIMARY KEY (id),
    FOREIGN KEY (product_id) REFERENCES products(id),
    FOREIGN KEY (matcher_id) REFERENCES matchers_obj(id),
    CHECK (type = 'debtor')
) INHERITS (rules);

--rollback DROP TABLE rules_debtor;
--rollback DROP TABLE rules_claim;
--rollback DROP TABLE rules_salary;
--rollback DROP TABLE rules;
