--liquibase formatted sql

--changeset shishkin:tables/operators logicalFilePath:/

SET search_path TO products;

CREATE TABLE operators (
    name varchar(100) PRIMARY KEY,
    description varchar(255) NULL
);

INSERT INTO operators(name, description) 
VALUES
	('greater', 'Матчер для сравнения чисел на >'),
	('greater_or_equals', 'Матчер для сравнения чисел на >='),
	('lower', 'Матчер для сравнения чисел на <'),
	('lower_or_equals', 'Матчер для сравнения чисел на <='),
	('equals', 'Матчер для сравнения чисел на =='),
	('not_equals', 'Матчер для сравнения чисел на !=');

COMMENT ON TABLE operators IS 'Операторы сравнения';
COMMENT ON COLUMN operators.name IS 'Название оператора сравнения';
COMMENT ON COLUMN operators.description IS 'Краткое описание оператора сравнения';

--rollback DROP TABLE operators;
