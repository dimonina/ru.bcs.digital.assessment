--liquibase formatted sql

--changeset shishkin:tables/matchers logicalFilePath:/ splitStatements:false

SET search_path TO products;

CREATE SEQUENCE matchers_id_seq;

CREATE TABLE matchers (
    id BIGINT PRIMARY KEY DEFAULT nextval('matchers_id_seq'),
	date_create timestamp NOT NULL DEFAULT now(),
	type VARCHAR(100) NOT NULL,
	value VARCHAR(255) NOT NULL,
	operator VARCHAR(255) NOT NULL REFERENCES operators(name)
);

CREATE TABLE matchers_num (
    CHECK (type = 'number'),
    CONSTRAINT value_should_be_numeric CHECK (value::NUMERIC IS NOT NULL),
	PRIMARY KEY (id),
    FOREIGN KEY (operator) REFERENCES operators(name)
) INHERITS (matchers);

CREATE TABLE matchers_obj (
    CHECK (type = 'object'),
    CONSTRAINT value_should_be_object CHECK (value IS NOT NULL),
	PRIMARY KEY (id),
    FOREIGN KEY (operator) REFERENCES operators(name)
) INHERITS (matchers);

COMMENT ON TABLE matchers IS 'Матчер для поиска соответствий';
COMMENT ON TABLE matchers_num IS 'Матчер для сравнения чисел';
COMMENT ON TABLE matchers_obj IS 'Матчер для сравнения любых значений';
COMMENT ON COLUMN matchers.id IS 'Уникальный идентификатор матчера';
COMMENT ON COLUMN matchers.date_create IS 'Дата создания матчера';
COMMENT ON COLUMN matchers.type IS 'Тип матчера';
COMMENT ON COLUMN matchers.value IS 'Граничное значение для матчера';
COMMENT ON COLUMN matchers.operator IS 'Оператор сравнения матчера';

--rollback DROP TABLE matchers_obj;
--rollback DROP TABLE matchers_num;
--rollback DROP TABLE matchers;

