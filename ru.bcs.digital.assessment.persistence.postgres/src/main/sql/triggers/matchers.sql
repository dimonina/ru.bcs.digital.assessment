--liquibase formatted sql

--changeset shishkin:triggers/matchers logicalFilePath:/ splitStatements:false

SET search_path TO products;

CREATE OR REPLACE FUNCTION matchers_insert_trigger()
RETURNS TRIGGER AS $$
BEGIN
    IF (NEW.type = 'number') THEN
        INSERT INTO products.matchers_num VALUES (NEW.*);
    ELSIF (NEW.type = 'object') THEN
        INSERT INTO products.matchers_obj VALUES (NEW.*);
    ELSE
        RAISE EXCEPTION 'Matchers types out of range.  Fix the matchers_insert_trigger() function!';
    END IF;
    RETURN NEW;
END;
$$
LANGUAGE plpgsql;

CREATE TRIGGER insert_matchers_trigger
    BEFORE INSERT ON matchers
    FOR EACH ROW EXECUTE PROCEDURE matchers_insert_trigger();

--rollback DROP TRIGGER insert_matchers_trigger;
--rollback DROP FUNCTION matchers_insert_trigger;
