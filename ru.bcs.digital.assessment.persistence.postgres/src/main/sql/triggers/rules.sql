--liquibase formatted sql

--changeset shishkin:triggers/rules logicalFilePath:/ splitStatements:false

SET search_path TO products;

CREATE OR REPLACE FUNCTION rules_insert_trigger()
RETURNS TRIGGER AS $$
BEGIN
    IF (NEW.type = 'salary') THEN
        INSERT INTO products.rules_salary VALUES (NEW.*);
    ELSIF (NEW.type = 'claim') THEN
        INSERT INTO products.rules_claim VALUES (NEW.*);
    ELSIF (NEW.type = 'debtor') THEN
        INSERT INTO products.rules_debtor VALUES (NEW.*);
    ELSE
        RAISE EXCEPTION 'Rule types out of range.  Fix the rules_insert_trigger() function!';
    END IF;
    RETURN NEW;
END;
$$
LANGUAGE plpgsql;

CREATE TRIGGER insert_rules_trigger
    BEFORE INSERT ON rules
    FOR EACH ROW EXECUTE PROCEDURE rules_insert_trigger();

--rollback DROP TABLE rules_debtor;
--rollback DROP TABLE rules_claim;
--rollback DROP TABLE rules_salary;
--rollback DROP TABLE rules;
--rollback DROP FUNCTION rules_insert_trigger;
