--liquibase formatted sql

--changeset shishkin:data/product_3 logicalFilePath:/ splitStatements:false

INSERT INTO products.products (id, name, percent, period)
VALUES (
	3, 
	'тестовый продукт 3', 
	12, 
	60
);

INSERT INTO products.matchers (id, type, value, operator)
VALUES (
	6,
	'number', 
	'25000', 
	'greater'
);
INSERT INTO products.rules (id, product_id, type, matcher_id)
VALUES (
	6, 
	3, 
	'salary', 
	6
);

INSERT INTO products.matchers (id, type, value, operator)
VALUES (
	7,
	'number', 
	'1000000', 
	'lower_or_equals'
);
INSERT INTO products.rules (id, product_id, type, matcher_id)
VALUES (
	7, 
	3, 
	'claim', 
	7
);

ALTER SEQUENCE products_id_seq RESTART WITH 4;
ALTER SEQUENCE matchers_id_seq RESTART WITH 8;
ALTER SEQUENCE rules_id_seq RESTART WITH 8;
