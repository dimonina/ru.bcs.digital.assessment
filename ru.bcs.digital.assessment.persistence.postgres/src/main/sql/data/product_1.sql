--liquibase formatted sql

--changeset shishkin:data/product_1 logicalFilePath:/ splitStatements:false

INSERT INTO products.products (id, name, percent, period)
VALUES (
	1, 
	'тестовый продукт 1', 
	6, 
	36
);

INSERT INTO products.matchers (id, type, value, operator)
VALUES (
	1,
	'number', 
	'200000', 
	'lower_or_equals'
);
INSERT INTO products.rules (id, product_id, type, matcher_id)
VALUES (
	1, 
	1, 
	'claim', 
	1
);

INSERT INTO products.matchers (id, type, value, operator)
VALUES (
	2,
	'number', 
	'50000', 
	'greater'
);
INSERT INTO products.rules (id, product_id, type, matcher_id)
VALUES (
	2, 
	1, 
	'salary', 
	2
);

INSERT INTO products.matchers (id, type, value, operator)
VALUES (
	3,
	'object', 
	'false', 
	'equals'
);
INSERT INTO products.rules (id, product_id, type, matcher_id)
VALUES (
	3, 
	1, 
	'debtor', 
	3
);
