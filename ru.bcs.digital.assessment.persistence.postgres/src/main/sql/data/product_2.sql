--liquibase formatted sql

--changeset shishkin:data/product_2 logicalFilePath:/ splitStatements:false

INSERT INTO products.products (id, name, percent, period)
VALUES (
	2, 
	'тестовый продукт 2', 
	15, 
	0
);

INSERT INTO products.matchers (id, type, value, operator)
VALUES (
	4,
	'number', 
	'0', 
	'greater'
);
INSERT INTO products.rules (id, product_id, type, matcher_id)
VALUES (
	4, 
	2, 
	'claim', 
	4
);

INSERT INTO products.matchers (id, type, value, operator)
VALUES (
	5,
	'object', 
	'false', 
	'equals'
);
INSERT INTO products.rules (id, product_id, type, matcher_id)
VALUES (
	5, 
	1, 
	'debtor', 
	5
);
