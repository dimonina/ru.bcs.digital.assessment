#!/bin/bash

sudo docker run \
	--name bcs-postgres \
	-e POSTGRES_USER=bcs \
	-e POSTGRES_PASSWORD=bcs \
	-e POSTGRES_DB=bcs \
	-p 5432:5432 \
	--rm \
	-d postgres:10-alpine