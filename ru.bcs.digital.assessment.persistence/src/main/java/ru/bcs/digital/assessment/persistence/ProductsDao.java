package ru.bcs.digital.assessment.persistence;

import java.util.List;

import ru.bcs.digital.assessment.api.Product;
import ru.bcs.digital.assessment.api.Rule;

/**
 * Data access object for persistence layer.
 */
public interface ProductsDao {

    /**
     * Retrieve all products from persistence layer.
     * 
     * @return all products from persistence layer, can't be null, but can be
     *         empty list
     */
    List<Product> getProducts();

    /**
     * Retrieve all rules for specified product from persistence layer.
     * 
     * @param productId identifier of product
     * @return all rules for specified product from persistence layer, can't be
     *         null, but can be empty list
     */
    List<Rule> getProductRules(long productId);

    /**
     * Add rule for specified product.
     * 
     * @param productId identifier of product, can't be null
     * @param rule rule to be added, can't be null
     * @return identifier of created rule, can't be null
     */
    Long addProductRule(long productId, Rule rule);

    /**
     * Delete rule from specified product.
     * 
     * @param productId identifier of product, can't be null
     * @param ruleId identifier of rule to be deleted, can't be null
     */
    void deleteProductRule(long productId, long ruleId);

    /**
     * Invalidate cache.
     * <p>
     * For testing purposes only.
     */
    public void invalidateCache();

}