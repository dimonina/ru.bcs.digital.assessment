package ru.bcs.digital.assessment.app.config;

import javax.sql.DataSource;

import org.jooq.DSLContext;
import org.jooq.SQLDialect;
import org.jooq.impl.DSL;
import org.jooq.impl.DefaultConfiguration;
import org.postgresql.ds.PGPoolingDataSource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import ru.bcs.digital.assessment.api.registry.MatchersFactory;
import ru.bcs.digital.assessment.api.registry.RulesFactory;
import ru.bcs.digital.assessment.database.daos.PostgresProductsDao;
import ru.bcs.digital.assessment.persistence.ProductsDao;

@Configuration
public class PostgresPersistenceConfig {

    @Value("${database.data_source_name:bcs-datasource}")
    private String dbDataSourceName;

    @Value("${database.host:localhost}")
    private String dbHost;

    @Value("${database.name:bcs}")
    private String dbName;

    @Value("${database.password:bcs}")
    private String dbPassword;

    @Value("${database.user:bcs}")
    private String dbUser;

    @Value("${database.dialect:POSTGRES_9_5}")
    private String dbDialect;

    @Value("${database.max_connections:10}")
    private int dbMaxConnections;

    @Autowired
    private MatchersFactory matchersFactory;

    @Autowired
    private RulesFactory rulesFactory;

    @Bean
    public DataSource dataSource() {
        PGPoolingDataSource result = new PGPoolingDataSource();
        result.setDataSourceName(dbDataSourceName);
        result.setServerName(dbHost);
        result.setDatabaseName(dbName);
        result.setUser(dbUser);
        result.setPassword(dbPassword);
        result.setMaxConnections(dbMaxConnections);
        return result;
    }

    @Bean
    public org.jooq.Configuration configuration() {
        org.jooq.Configuration result = new DefaultConfiguration();
        result.set(SQLDialect.valueOf(dbDialect));
        result.set(dataSource());
        return result;
    }

    @Bean
    public DSLContext dslContext() {
        return DSL.using(configuration());
    }

    @Bean
    public ProductsDao productsDao() {
        return new PostgresProductsDao(dslContext(), matchersFactory, rulesFactory);
    }

}
