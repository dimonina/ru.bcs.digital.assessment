package ru.bcs.digital.assessment.app.model;

import lombok.Value;

@Value
public class SuccessResponse {

    private final int status;

    private final String message;

    private final Long id;

}
