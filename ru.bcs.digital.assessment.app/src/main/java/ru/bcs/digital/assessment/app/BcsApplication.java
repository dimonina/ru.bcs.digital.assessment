package ru.bcs.digital.assessment.app;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class BcsApplication {

    public static void main(String[] args) {
        SpringApplication.run(BcsApplication.class, args);
    }

}
