package ru.bcs.digital.assessment.app.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import com.fasterxml.jackson.annotation.JsonAutoDetect.Visibility;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.annotation.PropertyAccessor;
import com.fasterxml.jackson.databind.ObjectMapper;

@Configuration
public class JsonConfig {

    /**
     * Default jackon behaviour seems to use both properties (getters and
     * setters) and fields to serialize and deserialize to json.
     * <p>
     * This configuration use the getters as the canonical source of
     * serialization config and thus don't want jackson to look at fields at
     * all.
     * <p>
     * Also all nullable values are ignored.
     * 
     * @return {@code ObjectMapper} instance
     */
    @Bean
    public ObjectMapper objectMapper() {
        ObjectMapper mapper = new ObjectMapper();
        mapper.setVisibility(PropertyAccessor.ALL, Visibility.PUBLIC_ONLY);
        mapper.setVisibility(PropertyAccessor.FIELD, Visibility.NONE);
        mapper.setSerializationInclusion(Include.NON_NULL);
        return mapper;
    }

}
