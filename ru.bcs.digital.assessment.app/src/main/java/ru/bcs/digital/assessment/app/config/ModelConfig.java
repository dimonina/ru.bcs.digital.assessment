package ru.bcs.digital.assessment.app.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import ru.bcs.digital.assessment.api.matchers.BigDecimalGreaterThenMatcher;
import ru.bcs.digital.assessment.api.matchers.BigDecimalGreaterThenOrEqualsMatcher;
import ru.bcs.digital.assessment.api.matchers.BigDecimalLowerThenMatcher;
import ru.bcs.digital.assessment.api.matchers.BigDecimalLowerThenOrEqualsMatcher;
import ru.bcs.digital.assessment.api.matchers.ObjectEqualsMatcher;
import ru.bcs.digital.assessment.api.matchers.ObjectNotEqualsMatcher;
import ru.bcs.digital.assessment.api.registry.MatchersFactory;
import ru.bcs.digital.assessment.api.registry.RulesFactory;
import ru.bcs.digital.assessment.api.registry.impl.MatchersFactoryImpl;
import ru.bcs.digital.assessment.api.registry.impl.RulesFactoryImpl;
import ru.bcs.digital.assessment.api.rules.ClaimRule;
import ru.bcs.digital.assessment.api.rules.DebtorRule;
import ru.bcs.digital.assessment.api.rules.SalaryRule;

@Configuration
public class ModelConfig {

    @Bean
    public RulesFactory rulesRegistry() {
        RulesFactoryImpl registry = new RulesFactoryImpl();
        registry.register(ClaimRule.TYPE, ClaimRule::new);
        registry.register(SalaryRule.TYPE, SalaryRule::new);
        registry.register(DebtorRule.TYPE, DebtorRule::new);
        return registry;
    }

    @Bean
    public MatchersFactory matchersRegistry() {
        MatchersFactoryImpl registry = new MatchersFactoryImpl();
        registry.register(BigDecimalGreaterThenMatcher.OPERATOR, BigDecimalGreaterThenMatcher::new);
        registry.register(
                BigDecimalGreaterThenOrEqualsMatcher.OPERATOR,
                BigDecimalGreaterThenOrEqualsMatcher::new);
        registry.register(BigDecimalLowerThenMatcher.OPERATOR, BigDecimalLowerThenMatcher::new);
        registry.register(
                BigDecimalLowerThenOrEqualsMatcher.OPERATOR,
                BigDecimalLowerThenOrEqualsMatcher::new);
        registry.register(ObjectEqualsMatcher.OPERATOR, ObjectEqualsMatcher::new);
        registry.register(ObjectNotEqualsMatcher.OPERATOR, ObjectNotEqualsMatcher::new);
        return registry;
    }

}
