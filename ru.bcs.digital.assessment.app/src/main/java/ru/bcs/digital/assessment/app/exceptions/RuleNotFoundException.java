package ru.bcs.digital.assessment.app.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value = HttpStatus.BAD_REQUEST, reason = "Rule with specified id not found")
public class RuleNotFoundException extends RuntimeException {

    /**
     * Serial Version UID.
     */
    private static final long serialVersionUID = 1L;

    public RuleNotFoundException() {
        super();
    }

    public RuleNotFoundException(String arg0, Throwable arg1) {
        super(arg0, arg1);
    }

    public RuleNotFoundException(String arg0) {
        super(arg0);
    }

    public RuleNotFoundException(Throwable arg0) {
        super(arg0);
    }

}
