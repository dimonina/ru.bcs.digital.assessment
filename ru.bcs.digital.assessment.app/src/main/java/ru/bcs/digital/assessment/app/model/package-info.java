/**
 * Package contains model object used internally in applications project.
 */
package ru.bcs.digital.assessment.app.model;