package ru.bcs.digital.assessment.app.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value = HttpStatus.BAD_REQUEST, reason = "Product with specified id not found")
public class ProductNotFoundException extends RuntimeException {

    /**
     * Serial Version UID.
     */
    private static final long serialVersionUID = 1L;

    public ProductNotFoundException() {
        super();
    }

    public ProductNotFoundException(String arg0, Throwable arg1) {
        super(arg0, arg1);
    }

    public ProductNotFoundException(String arg0) {
        super(arg0);
    }

    public ProductNotFoundException(Throwable arg0) {
        super(arg0);
    }

}
