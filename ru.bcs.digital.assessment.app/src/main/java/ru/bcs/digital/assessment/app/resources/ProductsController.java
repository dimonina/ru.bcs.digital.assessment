package ru.bcs.digital.assessment.app.resources;

import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import ru.bcs.digital.assessment.api.Client;
import ru.bcs.digital.assessment.api.Product;
import ru.bcs.digital.assessment.api.Rule;
import ru.bcs.digital.assessment.app.exceptions.ProductNotFoundException;
import ru.bcs.digital.assessment.app.exceptions.RuleNotFoundException;
import ru.bcs.digital.assessment.app.model.SuccessResponse;
import ru.bcs.digital.assessment.persistence.ProductsDao;

@RestController
@RequestMapping("/products")
public class ProductsController {

    private static final String APPLICATION_JSON_VALUE = "application/json";

    @Autowired
    private ProductsDao dao;

    /**
     * Retrieve all supported products.
     * 
     * @return all supported products
     */
    @GetMapping
    public List<Product> getAllProducts() {
        return dao.getProducts();
    }

    /**
     * Retrieve all rules accepted for product with specified id.
     * 
     * @param productId the unique identifier of product
     * @return all rules accepted for product with specified id
     */
    @GetMapping("/{product_id}/rules")
    public List<Rule> getProductRules(@PathVariable("product_id") Long productId) {
        return dao.getProductRules(productId);
    }

    @PostMapping(value = "/{product_id}/rules", consumes = APPLICATION_JSON_VALUE, produces = APPLICATION_JSON_VALUE)
    public SuccessResponse postProductRule(@PathVariable("product_id") Long productId,
            @RequestBody Rule rule) {
        try {
            Long id = dao.addProductRule(productId, rule);
            return new SuccessResponse(200, "OK", id);
        } catch (IllegalArgumentException e) {
            throw new ProductNotFoundException(e);
        }
    }

    @DeleteMapping("/{product_id}/rules/{rule_id}")
    public SuccessResponse deleteProductRule(@PathVariable("product_id") Long productId,
            @PathVariable("rule_id") Long ruleId) {
        try {
            dao.deleteProductRule(productId, ruleId);
            return new SuccessResponse(200, "OK", 0L);
        } catch (IllegalArgumentException e) {
            throw new RuleNotFoundException(e);
        }
    }

    @PostMapping("/apply")
    public List<Product> applyClient(@RequestBody Client client) {
        return dao.getProducts().stream().filter(product -> {
            List<Rule> rules = dao.getProductRules(product.getId());
            return rules.stream().allMatch(rule -> rule.test(client));
        }).collect(Collectors.toList());
    }

    @GetMapping("/invalidate_cache")
    public SuccessResponse invalidateCache() {
        dao.invalidateCache();
        return new SuccessResponse(200, "OK", 0L);
    }

}